from main import *
import os
from collections import Counter
from sklearn.decomposition import NMF
import numpy as np

DATA_FOLDER = "active1000"
files = os.listdir(DATA_FOLDER)
ARBITRARY_INDEX = 0
filepath = os.path.join(DATA_FOLDER, files[ARBITRARY_INDEX])


data = load_data(DATA_FOLDER)
ratings = load_dataset(data)
#print(data)
#print(ratings)
#print(type(ratings))

class MFAlgorithm():

    def __init__(self, data, featureCount):
        self.data = data
        self.featureCount = featureCount
        self.userCount = data.shape[0]
        self.itemCount = data.shape[1]
        #randomly ininitialize the features matrices for users and items
        self.userFeatures = np.random.uniform(low = 0.1, high = 0.9, size = (self.userCount, self.featureCount))
        self.itemFeatures = np.random.uniform(low = 0.1, high = 0.9, size = (self.featureCount, self.itemCount))

    
    def MSE(self):
        """
        Mean Squared Error. It compares the dor product of user.feature row and feature-item column to user-item cell
        """
        matrixProduct = np.matmul(self.userFeatures, self.itemFeatures)
        res = np.sum((self.data - matrixProduct)**2)
        return res

    
    def singleGradient(self, userRowIndex, itemCollumnIndex, userIndex=None, itemIndex=None):
        """
        Computes gradient of single user-item cell to a single user-feature or feature-item cell
        """
        if userIndex != None and itemIndex != None:
            return "Too many elements"
        elif userIndex == None and itemIndex == None:
            return "not enough elements"
        else:
            userRow = self.userFeatures[userRowIndex, :]
            itemCollumn = self.itemFeatures[:, itemCollumnIndex]
            UIRating = float(self.data[userRowIndex, itemCollumnIndex])
            prediction = float(np.dot(userRow, itemCollumn))
            if userIndex != None:
                rowElement = float(itemCollumn[userIndex])
                gradient = (UIRating - prediction)*rowElement*2
            else:
                collumnElement = float(userRow[itemIndex])
                gradient = (UIRating - prediction)*collumnElement*2
        return gradient
    

    def userFeatureGradient(self, userRow, userIndex):
        sum = 0
        for i in range(0, self.itemCount):
            sum += self.singleGradient(userRowIndex=userRow, itemCollumnIndex=i, userIndex=userIndex)
        res = sum / self.itemCount
        return res
    

    def itemFeatureGradient(self, itemCollumn, itemIndex):
        sum = 0
        for i in range(0, self.userCount):
            sum += self.singleGradient(userRowIndex=i, itemCollumnIndex=itemCollumn, itemIndex=itemIndex)
        res = sum / self.itemCount
        return res
    

    def updateUserFeatures(self, learningRate):
        for i in range(0, self.userCount):
            for j in range(0, self.featureCount):
                self.userFeatures[i, j] += learningRate*self.userFeatureGradient(userRow=i, userIndex=j)
    
    def updateItemFeatures(self, learningRate):
        for i in range(0, self.featureCount):
            for j in range(0, self.itemCount):
                self.itemFeatures[i, j] += learningRate*self.itemFeatureGradient(itemCollumn=j, itemIndex=i)


    def trainModel(self, learningRate=0.1, iterations = 1000):
        for i in range(iterations):
            self.updateUserFeatures(learningRate=learningRate)
            self.updateItemFeatures(learningRate=learningRate)
            if i % 50 == 0:
                print(self.MSE())


model = MFAlgorithm(ratings, 2)
model.trainModel()


#https://towardsdatascience.com/recommender-systems-in-python-from-scratch-643c8fc4f704

#nmf = NMF()
#ratings = numpy.array(ratings)
#W = nmf.fit_transform(ratings)
#H = nmf.components_
#nR = numpy.dot(W, H)
#print(nR)
