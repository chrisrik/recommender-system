import json
import os
import pandas as pd
import numpy as np
import nimfa

def load_data(path):
    """
        Load events from files and convert to dataframe. (From example project)
    """
    map_lst=[]
    for f in os.listdir(path):
        file_name=os.path.join(path,f)
        if os.path.isfile(file_name):
            for line in open(file_name):
                obj = json.loads(line.strip())
                if not obj is None:
                    map_lst.append(obj)
    return pd.DataFrame(map_lst) 

def load_dataset(df):
    """
        Convert dataframe to user-item-interaction matrix, which is used for 
        Matrix Factorization based recommendation.
        In rating matrix, clicked events are refered as 1 and others are refered as 0.  (From example project)
    """
    df = df[~df['documentId'].isnull()]
    df = df.drop_duplicates(subset=['userId', 'documentId']).reset_index(drop=True)
    df = df.sort_values(by=['userId', 'time'])
    n_users = df['userId'].nunique()
    n_items = df['documentId'].nunique()

    ds = np.zeros((n_users, n_items))
    new_user = df['userId'].values[1:] != df['userId'].values[:-1]
    new_user = np.r_[True, new_user]
    df['uid'] = np.cumsum(new_user)
    item_ids = df['documentId'].unique().tolist()
    new_df = pd.DataFrame({'documentId':item_ids, 'tid':range(1,len(item_ids)+1)})
    df = pd.merge(df, new_df, on='documentId', how='outer')
    df_ext = df[['uid', 'tid']]
    
    for row in df_ext.itertuples():
        ds[row[1]-1, row[2]-1] = 1.0
    return ds

def train_test_split(ds, fraction=0.2):
    """Leave out a fraction of dataset for test use"""
    test = np.zeros(ds.shape)
    train = ds.copy()
    for user in range(ds.shape[0]):
        size = int(len(ds[user, :].nonzero()[0]) * fraction)
        test_ds = np.random.choice(ds[user, :].nonzero()[0], 
                                        size=size, 
                                        replace=False)
        train[user, test_ds] = 0.
        test[user, test_ds] = ds[user, test_ds]
    return train, test


def print_info(fit, idx=None):
    """
    Print to stdout info about the factorization.
    
    :param fit: Fitted factorization model.
    :type fit: :clast:`nimfa.models.mf_fit.Mf_fit`
    :param idx: Name of the matrix (coefficient) matrix. Used only in the multiple NMF model. Therefore in factorizations 
                that follow standard or nonsmooth model, this parameter can be omitted. Currently, SNMNMF implements 
                multiple NMF model.
    :type idx: `str` with values 'coef' or 'coef1' (`int` value of 0 or 1, respectively) 
    """
    print("=================================================================================================")
    print("Factorization method:", fit.fit)
    print("Initialization method:", fit.fit.seed)
    print("Basis matrix W: ")
    #print(__fact_factor(fit.basis()))
    print("Mixture (Coefficient) matrix H%d: " % (idx if idx != None else 0))
    #print(__fact_factor(fit.coef(idx)))
    print("Distance (Euclidean): ", fit.distance(metric='euclidean', idx=idx))
    # We can access actual number of iteration directly through fitted model.
    # fit.fit.n_iter
    print("Actual number of iterations: ", fit.summary(idx)['n_iter'])
    # We can access sparseness measure directly through fitted model.
    # fit.fit.sparseness()
    print("Sparseness basis: %7.4f, Sparseness mixture: %7.4f" % (fit.summary(idx)['sparseness'][0], fit.summary(idx)['sparseness'][1]))
    # We can access explained variance directly through fitted model.
    # fit.fit.evar()
    print("Explained variance: ", fit.summary(idx)['evar'])
    # We can access residual sum of squares directly through fitted model.
    # fit.fit.rss()
    print("Residual sum of squares: ", fit.summary(idx)['rss'])
    # There are many more ... but just cannot print out everything =] and some measures need additional data or more runs
    # e.g. entropy, predict, purity, coph_cor, consensus, select_features, score_features, connectivity
    print("=================================================================================================")


def MF(df):
    # get rating matrix
    ds = load_dataset(df)
    # split ratings into train and test sets
    train, test = train_test_split(ds, fraction=0.2)
    # create model
    bmf = nimfa.Bmf(train, seed="nndsvd", rank=10, max_iter=12, lambda_w=1.1, lambda_h=1.1)
    bmf_fit = bmf()
    print_info(bmf_fit)


df = load_data("active1000")
MF(df)
#print(result)