import os
from collections import Counter

DATA_FOLDER = os.curdir+'./active1000'
files = os.listdir(DATA_FOLDER)
print(files)

import json

events = []
for f in files:
    filepath = os.path.join(DATA_FOLDER, f)
    for line in open(filepath):
        events.append(json.loads(line.strip()))
    print(".", end = '')
print("")
print("finished reading files")

df = []
with open("document_frequency.txt", "r") as file:
    file_lines = file.read()
    df = file_lines.split("\n")
    file.close()

df_dict = {}
for item in df:
    item_split = item.split(": ")
    item_split[0] = item_split[0].strip("'")
    df_dict[item_split[0]] = item_split[1]

print(df_dict["stor"])