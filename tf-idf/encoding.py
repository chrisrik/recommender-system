import os
from collections import Counter

DATA_FOLDER = os.curdir+'active1000'
files = os.listdir(DATA_FOLDER)
print(files)

import json

events = []
for f in files:
    filepath = os.path.join(DATA_FOLDER, f)
    for line in open(filepath):
        events.append(json.loads(line.strip()))
    print(".", end = '')
print("")
print("finished reading files")

stopwords = []
with open("stopwords.txt", "r") as file:
    file_lines = file.read()
    stopwords = file_lines.split("\n")
    file.close()

df = []
documents = []
x = 0
for e in events:
    x += 1
    title = e.get("title")
    doc_id = e.get("documentId")
    if title is not None and doc_id not in documents:
        documents.append(doc_id)
        title = str(title.strip(".,"))
        words = title.lower().split(" ")
        for word in words:
            if word not in stopwords:
                df.append(word)
    if x % 10000 == 0: print(".", end = '')
print("")
print("finished listing words")

out_string = str(Counter(df))[9:-2]
out = out_string.split(", ")

#raw_text = json.dumps(events[ARBITRARY_INDEX], indent=4)
#print(raw_text)

with open('document_frequency.txt', 'w') as f:
    file_lines = "\n". join(out)
    f.write(file_lines)