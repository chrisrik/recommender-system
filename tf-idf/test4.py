from sklearn.feature_extraction.text import TfidfVectorizer
import os
from collections import Counter
import pandas as pd
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import word_tokenize
import json
from sklearn.metrics.pairwise import cosine_similarity

DATA_FOLDER = os.curdir+"/active1000"
files = os.listdir(DATA_FOLDER)
stemmer = SnowballStemmer("norwegian")

def tokenize(text):
    tokens = [word for word in word_tokenize(text) if len(word) > 1]
    stems = [stemmer.stem(item) for item in tokens]
    return stems

def main():
    events = []
    for f in files:
        filepath = os.path.join(DATA_FOLDER, f)
        for line in open(filepath):
            event = json.loads(line.strip())
            if str(event.get("title")) != "None" and str(event.get("title")) != "":
                events.append(event)
        print(".", end = '')
    print("")
    print("finished reading files")

    docs = []
    print(events[0].get("title"))
    for e in events:
        docs.append(str(e.get("title")))
    docs = list(dict.fromkeys(docs))
    print(len(docs))

    stopwords = []
    with open("stopwords.txt", "r") as file:
        file_lines = file.read()
        stopwords = file_lines.split("\n")
        file.close()

    tfidf_vectorizer = TfidfVectorizer(use_idf=True, stop_words=stopwords, tokenizer=tokenize)
    # just send in all your docs here
    fitted_vectorizer = tfidf_vectorizer.fit(docs)
    tfidf_vectorizer_vectors = fitted_vectorizer.transform(docs)

    # get the first vector out (for the first document) 
    first_vector_tfidfvectorizer = tfidf_vectorizer_vectors[0] 
    # place tf-idf values in a pandas data frame 
    df = pd.DataFrame(first_vector_tfidfvectorizer.T.todense(), index=tfidf_vectorizer.get_feature_names(), columns=["tfidf"])
    print(df.sort_values(by=["tfidf"],ascending=False))

    tfidf_matrix = tfidf_vectorizer.fit_transform(docs)
    print('Dimension of feature vector: {} - (documents, unique words)'.format(tfidf_matrix.shape))

    # measure similarity of two articles with cosine similarity
    cosine_sim = cosine_similarity(tfidf_matrix, tfidf_matrix)
    print(cosine_sim)

if __name__ == "__main__":
    main()