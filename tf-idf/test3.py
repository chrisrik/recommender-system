from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer
import os
from collections import Counter
import pandas as pd

DATA_FOLDER = os.curdir+"/active1000"
files = os.listdir(DATA_FOLDER)
print(files)

import json
events = []
for f in files:
    filepath = os.path.join(DATA_FOLDER, f)
    for line in open(filepath):
        events.append(json.loads(line.strip()))
    print(".", end = '')
print("")
print("finished reading files")

docs = []
for e in events:
    docs.append(str(e.get("title")))
print(len(docs))
docs = list(dict.fromkeys(docs))

cv = CountVectorizer()
word_count_vector=cv.fit_transform(docs)

print(len(docs))
print(word_count_vector.shape)

tfidf_transformer = TfidfTransformer(smooth_idf=True,use_idf=True) 
tfidf_transformer.fit(word_count_vector)

# print idf values 
df_idf = pd.DataFrame(tfidf_transformer.idf_, index=cv.get_feature_names(),columns=["idf_weights"]) 
# sort ascending 
print(df_idf.sort_values(by=['idf_weights']))
